#include "numbergenerator.h"

numberGenerator::numberGenerator(ofstream &file, uint totalLineas,
                                 ushort totalColumnas,ushort dataSize){

    this->file=&file;
    this->totalLineas=totalLineas;
    this->contadorLineas=0;
    this->totalColumnas=totalColumnas;
    this->dataSize=dataSize;

    stringstream tempSemilla {};
    tempSemilla <<static_cast<ushort>(clock());

    string m {tempSemilla.str()[2]};
    semilla = new uint {static_cast<uint>(atoi(m.c_str()))};
    numeroMaximo = maximos[this->dataSize-1];
    generarDatos();

}
numberGenerator:: numberGenerator(const numberGenerator &s){

    semilla= new uint(*s.semilla);
    totalLineas=s.totalLineas;
    file =s.file;
    contadorLineas =s.contadorLineas;
    totalColumnas =s.totalColumnas;
    dataSize =s.dataSize;
    numeroMaximo=s.numeroMaximo;
    numeroMinimo=s.numeroMinimo;
    generardor =s.generardor;
 }
void numberGenerator::generarDatos(){

    if (this->dataSize% 2==0)
       generardor= &numberGenerator::generarNumeroWP;

    else
        generardor =&numberGenerator::generarNumeroNP;


    while (this->contadorLineas < this->totalLineas){

        this->generarLinea();
        this->contadorLineas++;
    }
}
void numberGenerator::generarLinea(){

    unsigned short contadorColumnas {};

    while (contadorColumnas<this->totalColumnas){

      *this->file<<(this->*generardor)() ;
      *this->file<<';';
      contadorColumnas++;
    }
    *this->file<<std::endl;
}
uint numberGenerator::generarNumeroNP(){

    return _glcNP(semilla,numeroMaximo);

}
uint numberGenerator::generarNumeroWP(){

    return _glcNP(semilla,numeroMaximo);

}
numberGenerator::~numberGenerator(){}
