#include "stringgenerator.h"

stringGenerator::stringGenerator(ofstream &file,unsigned int totalLineas,
                                 unsigned short totalColumnas,unsigned short dataSize){

    this->file=&file;
    this->totalLineas=totalLineas;
    this->semilla=static_cast<unsigned long>(clock());
    this->contadorLineas=0;
    this->totalColumnas=totalColumnas;
    this->dataSize=dataSize;

    this->generarDatos();
}

stringGenerator& stringGenerator::operator=(const stringGenerator &&s){

    this->semilla =move(s.semilla);
    this->totalLineas=move(s.totalLineas);
    this->file =move(s.file);
    this->contadorLineas =move(s.contadorLineas);
    this->totalColumnas =move(s.totalColumnas);
    this->dataSize =move(s.dataSize);

    return *this;

}
void stringGenerator::generarDatos(){

    while (this->contadorLineas < this->totalLineas){

        this->generarLinea();
        this->contadorLineas++;
    }
}
void stringGenerator::generarLinea(){

    unsigned short contadorColumnas {};

    while (contadorColumnas<this->totalColumnas){


        *this->file<<this->generarSecuencia(this->dataSize);
        *this->file<<';';
        contadorColumnas++;
    }

    *this->file<<std::endl;
}
string &&stringGenerator::generarSecuencia(unsigned int sizeCadena){

    stringstream cadenaAleatoria{};
    char *contenedor {new char[sizeCadena]};
    unsigned int i{0};

    while( i  <= sizeCadena){

        contenedor[i]=this->datos[glc()% 49];
        i++;
    }

    cadenaAleatoria<<contenedor;
    delete [] contenedor;

    return move(cadenaAleatoria.str());
}
unsigned int&& stringGenerator::glc(){

    this->semilla=_glc(this->semilla);
    return move(static_cast<unsigned int>(this->semilla));
}
stringGenerator::~stringGenerator(){}
