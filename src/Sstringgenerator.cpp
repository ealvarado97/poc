#include "stringgenerator.h"

stringGenerator::stringGenerator(ofstream &file,unsigned int totalLineas){

    this->file=&file;
    this->totalLineas=totalLineas;

}
void stringGenerator::ejecutor(){

    int b;
    pthread_t p1;
    b=pthread_create(&p1,nullptr,run,static_cast<void*>(this));
    if(b!=0)
        cout<<"error"<<endl;

}
stringGenerator::stringGenerator(stringGenerator &s){

    this->semilla=s.semilla;
    this->totalLineas=s.totalLineas;
    this->file =s.file;
}
bool stringGenerator::generarDatos(){

    while (stringGenerator::contadorLineas < this->totalLineas){

        this->generarLinea(10);
        stringGenerator::contadorLineas++;
    }
    return  true;
}
bool stringGenerator::generarLinea(unsigned int columnas){

    unsigned int contadorColumnas =0;
    bool bandera=false;

    while (contadorColumnas<columnas){

        *this->file<<this->generarSecuencia(100);
        *this->file<<';';
        contadorColumnas++;
        bandera=true;
    }

    *this->file<<std::endl;
    return  bandera;
}
string stringGenerator::generarSecuencia(unsigned int sizeCadena){

    stringstream cadenaAleatoria;

    unsigned int i=0;

    while(i <= sizeCadena){

        cadenaAleatoria <<this->datos[rand()%49];
        i++;
    }

    return  cadenaAleatoria.str();

}
unsigned int stringGenerator::glc(){

    this->semilla=_glc(this->semilla);
    return  static_cast<unsigned int>(this->semilla);
}
void *stringGenerator::run(void *dato){

    stringGenerator *generator;
    generator =static_cast<stringGenerator*>(dato);

    generator->generarDatos();
    return nullptr;

}
stringGenerator::~stringGenerator(){}
