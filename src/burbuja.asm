global _supportCPUID:function
global _supportRDRAND:function
global _generateRDRAND:function
global _glc:function
global _glcNP:function
global _glcWP:function


section .text

  ;global _start

 ; _start:
;  call _supportCPUI


  _supportCPUID:

   xor r11, r11
   xor rax, rax

   pushfq    ;Guarda el estado de las banderas
   pop r11   ;Obtiene el esatdo  de las banderas

   test r11, 200000h ;Verifica el bit 21

   jnz .support
   ret

   .support:
    mov rax, 1d

   ret

  _supportRDRAND:
;**********************************************************

   mov rax, 1d ;EAX=1: Processor Info and Feature Bits
   cpuid

   and ecx , 40000000h ; Verifica el bit 30 RDRAND
   jz .support ;ZF=1

   xor eax,eax
   ret

   .support:
     mov rax ,1d

   ret
;***********************************************************
  _generateRDRAND:

    xor rdx, rdx
    xor rax, rax

    rdrand ax
    jnc .nogenerate

    mov cx ,49d
    div cx
    xchg ax, dx
    ret

    .nogenerate:
      xor eax, eax

    ret
;************************************************************
_glc:

  ;edi parametro semilla
  ;Genera numeros aleatorios para cadenas de texto

  xor edx, edx
  mov eax, edi          ;semilla

  mov r11d, 0xbc8f      ;48271
  mul r11d              ;R=(48271*semilla)

  add eax, 0x3039       ;L=R+12345
  and eax,0x7fffffff    ;semilla=L mod 0x7fffffff (2^31)-1

  ret
;**************************************************************
_glcNP:

 ;*edi parametro semilla
 ;rsi numero maximo(cantidad de digitos)
 ;rdx numero minimo

 ;Genera numeros aleatorios para numeros enteros

  push r11
  push r8

  mov r8, rdx

  xor rdx, rdx
  mov rax, [rdi]             ;semilla is pointer

  mov r11, 0x5DEECE66D       ;25214903917
  mul r11                    ;R=(25214903917*semilla)

  add rax, 11                ;L=R+11
  and rax, 0xFFFFFFFFFFFF    ;K=L mod (2^48-1)

  mov [rdi], rax             ;*semilla=K

  xor rdx,rdx

  div rsi                    ;K mod numeroMaximo
  xchg rax, rdx

  cmp rax, rdx               ;se Verifica el largo del numero
  jnl .numeroValido

  add r8, rax                ;se ajusta el numero para que
                             ;tenga el largo minimo

  .numeroValido:             ;el numero tiene el largo
    nop                      ;minimo requerido


  pop r8
  pop r11

  ret
;**************************************************************
_glcWP:

 ;edi parametro semilla
 ;rsi numero maximo(cantidad de digitos)
 ;rdx numero minimo

 ;Genera numeros aleatorios para cadenas de texto

 xor edx, edx
 mov eax, edi          ;semilla

 mov r11d, 0xbc8f      ;48271
 mul r11d              ;R=(48271*semilla)

 add eax, 0x3039       ;L=R+12345
 and eax,0xffffffff    ;semilla=L mod 0x7fffffff (2^32)-1

 ;div esi
 ;xchg rax, rdx
 ret






