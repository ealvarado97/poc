#ifndef NUMBERGENERATOR_H
#define NUMBERGENERATOR_H

#include<iostream>
#include<fstream>
#include<ctime>
#include<sstream>
#include <math.h>

using namespace  std;

extern "C" uint _glcNP(uint*,uint);
extern "C" uint _glcWP(uint*,uint);

class numberGenerator{

public:

    numberGenerator(ofstream& ,uint,ushort,ushort);
    numberGenerator(const numberGenerator &);
    void generarDatos();

    numberGenerator& operator=(const numberGenerator &&)=delete;
    uint *semilla;
    ~numberGenerator();

private:

    uint dataSize; //largo del numero,
    uint totalColumnas; //cantidad de columnas por linea
    uint contadorLineas;
    uint totalLineas;   //numero de lineas a generar
    //uint *semilla;     //numero aleatorio de inicio

    ofstream *file;

    uint maximos [9]={9,99,999,9999,99999,999999,9999999,99999999,
                    999999999};

    uint minimos [9]={1,10,100,1000,10000,100000,1000000,10000000,
                     100000000};

    uint numeroMaximo; //numero mas grande a generar
    uint numeroMinimo; //numero mas pequeño a generar

    uint generarNumeroWP();//numero potencia de 2
    uint generarNumeroNP();//numero no potencia de 2

    void generarLinea();

    uint (numberGenerator::*generardor)();


};

#endif // NUMBERGENERATOR_H
