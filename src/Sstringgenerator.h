#ifndef STRINGGENERATOR_H
#define STRINGGENERATOR_H

#include<iostream>
#include<fstream>
#include<pthread.h>
#include<ctime>
#include<sstream>

using namespace std;
extern "C" unsigned long _glc(unsigned long);

class stringGenerator{

public:

   inline static unsigned int contadorLineas=0;

    stringGenerator(ofstream&,unsigned int);
    stringGenerator(stringGenerator&);
    bool generarDatos();
    void ejecutor();
   ~stringGenerator();

private:


    unsigned int totalLineas;
    unsigned long semilla;
    ofstream *file;

    char datos[50] ={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','r','o','s','t','p','u','v','w','x','y','z',
                  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','R','O','S','T','P','U','V','W','X','Y','z'};


    string generarSecuencia(unsigned int);
    bool   generarLinea(unsigned int);
    unsigned int glc();
    static void *run(void *);




};

#endif // STRINGGENERATOR_H
