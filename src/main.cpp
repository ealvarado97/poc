#include<fstream>
#include<ctime>
#include "stringgenerator.h"
#include<memory>
#include "numbergenerator.h"

void pocC();//generadorStrings
void pocN();//generadorEnteros



static unsigned int totalLineas =1000000;

std::ofstream *salida=nullptr;

int main(){

    salida= new ofstream("salida.txt");
    pocN();

    return 0;

}
void pocC(){

    unsigned long t0, t1;
    t0=static_cast<unsigned long>(clock());

    std::unique_ptr<stringGenerator> c {std::make_unique<stringGenerator>(*salida,totalLineas,10,100)};

    t1 = static_cast<unsigned long>(clock()) ;
    double time = (double(t1-t0)/CLOCKS_PER_SEC);

    *salida<< "Execution Time: " << time << std::endl;

}
void pocN(){

    unsigned long t0, t1;
    t0=static_cast<unsigned long>(clock());

    std::unique_ptr<numberGenerator> c {std::make_unique<numberGenerator>(*salida,totalLineas,10,8)};

    t1 = static_cast<unsigned long>(clock()) ;
    double time = (double(t1-t0)/CLOCKS_PER_SEC);

    *salida<< "Execution Time: " << time << std::endl;

}


