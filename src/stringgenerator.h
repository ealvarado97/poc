#ifndef STRINGGENERATOR_H
#define STRINGGENERATOR_H

#include<iostream>
#include<fstream>
#include<ctime>
#include<sstream>

using namespace std;

extern "C" unsigned long _glc(unsigned long);

class stringGenerator{

public:

    stringGenerator(ofstream&,unsigned int,unsigned short,unsigned short) ;
    stringGenerator(stringGenerator&)=delete ;
    void generarDatos();
    stringGenerator& operator=(const stringGenerator&&);

   ~stringGenerator();

private:
    unsigned short dataSize; //cantidad de caracteres de la cadena
    unsigned short totalColumnas; //cantidad de columnas por linea
    unsigned int contadorLineas;
    unsigned int totalLineas;   //numero de lineas a generar
    unsigned long semilla;     //numero aleatorio de inicio
    ofstream *file;

    char datos[50] ={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','r','o','s','t','p','u','v','w','x','y','z',
                  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','R','O','S','T','P','U','V','W','X','Y','z'};


    string&& generarSecuencia(unsigned int);
    void   generarLinea();
    unsigned int &&glc();

};

#endif // STRINGGENERATOR_H
